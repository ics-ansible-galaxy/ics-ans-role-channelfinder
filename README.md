# ics-ans-role-channelfinder

Ansible role to install ChannelFinder.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml
channelfinder_network: channelfinder-network
channelfinder_es_container_name: elasticsearch
channelfinder_es_image_name: docker.elastic.co/elasticsearch/elasticsearch
channelfinder_es_image_tag: 6.8.23
channelfinder_es_image: "{{ channelfinder_es_image_name }}:{{ channelfinder_es_image_tag }}"
channelfinder_es_volume: channelfinder-es-data
channelfinder_container_name: channelfinder
channelfinder_image_name: registry.esss.lu.se/ics-software/channelfinderservice
channelfinder_image_tag: latest
channelfinder_image: "{{ channelfinder_image_name }}:{{ channelfinder_image_tag }}"
channelfinder_frontend_rule: "Host:{{ ansible_fqdn }}"
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-channelfinder
```

## License

BSD 2-clause
