import os
import testinfra.utils.ansible_runner
import time

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_container(host):
    with host.sudo():
        containers = host.docker.get_containers(name=["channelfinder", "elasticsearch", "traefik_proxy"])
        assert all(container.is_running for container in containers)


def test_channels(host):
    up = False
    for i in range(60):
        cmd = host.run('curl --insecure --fail --silent https://ics-ans-role-channelfinder-default/')
        if cmd.rc == 0:
            up = True
            break
        time.sleep(2)
    if not up:
        raise RuntimeError('Timed out waiting for application to start.')
    assert host.run('curl -k https://ics-ans-role-channelfinder-default/ChannelFinder/resources/channels').stdout == '[]'
    assert host.run('curl -k https://ics-ans-role-channelfinder-default/ChannelFinder/resources/tags').stdout == '[]'
    assert host.run('curl -k https://ics-ans-role-channelfinder-default/ChannelFinder/resources/properties').stdout == '[]'
